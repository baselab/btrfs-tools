#!/usr/bin/env python3
"""
BTRFS tools
Inspect and manage btrfs file system
"""

import sys
from configparser import SafeConfigParser
from lib.options import Options
from lib.utils import Inspect

def run():
    """
    Main function
    """

    config = "config.ini"
    readconf = SafeConfigParser()
    readconf.read(config)
    description = readconf.get("About", "description")
    homepage = readconf.get("About", "homepage")

    options = Options.parser(description, homepage)
    args = options.parse_args()

    if args.u:
        Inspect.df(args.mountpoint)
    elif args.v:
        Inspect.subvol(args.mountpoint)
    elif args.s:
        print("List snapshots")

if __name__ == '__main__':
    run()
