# Parse the options

import argparse

class Options():
    def parser(description, homepage):
        parser = argparse.ArgumentParser(prog="btrtool", description=description, epilog=homepage)
        group = parser.add_mutually_exclusive_group()
        group.add_argument("-u", help="show filesystem usage", action="store_true")
        group.add_argument("-v", help="list subvolumes", action="store_true")
        group.add_argument("-s", help="list snapshots", action="store_true")
        parser.add_argument("mountpoint", help="filesystem mountpoint")
        return parser
