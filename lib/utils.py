# Do things

import btrfs

class Inspect():
    """
    Filesystem space usage
    """

    def df(fs):
        """ Simple df """
        spaces = btrfs.FileSystem(fs).space_info()
        for i in spaces:
            print(i)

    def subvol(fs):
        """ List subvolumes """
        subvols = btrfs.FileSystem(fs).subvolumes()
        for i in subvols:
            print(i)
